<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Blog;
use App\user;
use App\Comment;
use Auth;

class BlogController extends Controller
{
    public function create()
    {
    	$user=Auth::User();
    	return view('form',compact('user'));
    }
    public function store(Request $request)
    {
    	$blog=new Blog();
    	$blog->title=$request->title;
    	$blog->detail=$request->Detail;
    	$blog->uid=$request->uid;
    	// $blog->uid=Auth::user()->id;

    	$blog->save();
    	//return $request->all();

    	return redirect('blog');
    }
    public function showblog()
    {   
    	$blog=Blog::latest()->get();

    	return view('blog',compact('blog'));
    }
    public function pblog($id)
    {
    	$blog=Blog::findOrFail($id);
    	$comments=$blog->comments()->latest()->get();
    	return view('allblog',compact('blog'));

    }
    public function comment($Request request)
    {
       $blog=Blog::findOrFail($request->bid);
        $comment=new Comment();
        //$comment->blogid=$request->bid;
        $comment->uid=Auth::User()->id;
        $comment->comments=$request->comment;
        //$comment->save();
        $blog->comments()->save($comment);
      //  $comment->uid=$request->uid;
        //return $comment;
       // $comment->save();


    }
}
