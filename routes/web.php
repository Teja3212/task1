<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('form','BlogController@create');
// Route::post('form','BlogController@store');
// Route::get('blog','Blogcontroller@showblog');
// Route::get('blog/{id}','Blogcontroller@pblog');

//Route::post('blog/{id}/show','Blogcontroller@comment');


Route::get('upload','uploadController@index');
Route::post('store','uploadController@store');
Route::get('show','uploadController@show');

