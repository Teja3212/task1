<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment', function (Blueprint $table) {
            $table->increments('id');
            $table->text('commets');
            $table->integer('blogid')->unsigned();
            $table->foreign('blogid')
                  ->references('id')
                  ->on('blog')
                  ->onDelete('cascade');
        

            $table->integer('uid')->unsigned();
            $table->foreign('uid')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment');
    }
}
